<div id="primary" class="col-xs-12">

    <div id="featured" class="col-xs-12">

        <img src="<?=get_template_directory_uri()?>/images/after_school.jpg" alt="After School" />

        <div class="caption col-xs-12">
            <a href="#">Find out about our programs for children</a>
        </div><!-- /caption -->

    </div><!-- /featured -->

</div><!-- /primary -->

<div id="secondary" class="col-xs-12">

    <div id="callout_1" class="callout col-sm-4 col-xs-12">
        <div class="col-xs-12">
        <a href="#"><img src="<?=get_template_directory_uri()?>/images/elementary_school.jpg" alt="Elementary School Programs" /></a>
        <div class="caption  col-xs-12">
                <a href="#">Elementary Programs</a>
        </div><!-- /caption -->
        </div>
    </div><!-- /callout -->

    <div id="callout_2" class="callout col-sm-4 col-xs-12">
    <div class="col-xs-12">
    <a href="#"><img src="<?=get_template_directory_uri()?>/images/middle_school.jpg" alt="Elementary School Programs" /></a>
        <div class="caption  col-xs-12">
                <a href="#">Middle School Programs</a>
        </div><!-- /caption -->
    </div>
    </div><!-- /callout -->

    <div id="callout_3" class="callout callout col-sm-4 col-xs-12">
        <div class="col-xs-12">
            <a href="#"><img src="<?=get_template_directory_uri()?>/images/high_school.jpg" alt="Elementary School Programs" /></a>
            <div class="caption  col-xs-12">
                    <a href="#">High School Programs</a>
            </div><!-- /caption -->
        </div>
    </div><!-- /callout -->


</div><!-- /secondary -->
